/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polsl.po.optim;

/**
 *
 * @author Pawel
 */
public class OptimRestriction {    

    // Constructor
    public OptimRestriction(String name, int positiveFactor, int negativeFactor, int expectedValue) {
        this.name = name;
        this.positiveFactor = positiveFactor;
        this.negativeFactor = negativeFactor;
        this.expectedValue = expectedValue;
    }
    
    // Members
    private String name;
    private int positiveFactor;
    private int negativeFactor;
    private int expectedValue;
    
    private int calculatedValue;

    // Getters and setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }    
    
    public int getPositiveFactor() {
        return positiveFactor;
    }

    public void setPositiveFactor(int positiveFactor) {
        this.positiveFactor = positiveFactor;
    }

    public int getNegativeFactor() {
        return negativeFactor;
    }

    public void setNegativeFactor(int negativeFactor) {
        this.negativeFactor = negativeFactor;
    }
    
    public int getExpectedValue() {
        return expectedValue;
    }

    public void setExpectedValue(int ExpectedValue) {
        this.expectedValue = ExpectedValue;
    }

    public int getCalculatedValue() {
        return calculatedValue;
    }

    public void setCalculatedValue(int calculatedValue) {
        this.calculatedValue = calculatedValue;
    }
    
    @Override
    public String toString(){
        return getName() + " (" + getExpectedValue() + "; " + getPositiveFactor()
                + "; " + getNegativeFactor() + ")";
    }

    public void generate(){
    }
}
