/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polsl.po.optim;

import java.util.List;
import org.jacop.constraints.*;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.*;
/**
 *
 * @author Pawel
 */
public class OptimProblem {
    private List<OptimRestriction> restrictions;
    private int resources;

    public List<OptimRestriction> getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(List<OptimRestriction> restrictions) {
        this.restrictions = restrictions;
    }

    public int getResources() {
        return resources;
    }

    public void setResources(int resources) {
        this.resources = resources;
    }
    
    public void solve(){
        // Every member needs to get a gift equal to minimum 1, so maximum resource spent on
        //  a single member is all the resources minus the number of members
        int maxVal = this.getResources() - this.getRestrictions().size() + 1;
        
        Store store = new Store();
        
        for(OptimRestriction restriction : getRestrictions()){
            IntVar nextValue = new IntVar(store, restriction.getName()+"Value", 1, maxVal);
        }
    }
}
