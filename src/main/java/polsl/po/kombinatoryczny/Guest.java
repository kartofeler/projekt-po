/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polsl.po.kombinatoryczny;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Andrzej
 */
public class Guest {

    private String name;
    private List<Guest> popularPeople;
    private List<Guest> dislikedPeople;

    public Guest(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Guest{" + "name=" + name + ", popularPerson=" + popularPeople + ", dislikedPerson=" + dislikedPeople + '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Guest> getPopularPeople() {
        return popularPeople;
    }

    public void addPopularPerson(Guest popularPerson) throws Exception {
        if (this.popularPeople == null) {
            this.popularPeople = new ArrayList<Guest>();
        }
        if(this.popularPeople.size() == 2 ) throw new Exception("Go�� nie mo�e siedzie� ko�o trzech os�b jednocze�nie!");
        this.popularPeople.add(popularPerson);
    }

    public void setPopularPeople(List<Guest> popularPeople) {
        this.popularPeople = popularPeople;
    }

    public List<Guest> getDislikedPeople() {
        return dislikedPeople;
    }

    public void addDislikedPerson(Guest dislikedPerson) {
        if (this.dislikedPeople == null) {
            this.dislikedPeople = new ArrayList<Guest>();
        }
        this.dislikedPeople.add(dislikedPerson);
    }

    public void setDislikedPeople(List<Guest> dislikedPeople) {
        this.dislikedPeople = dislikedPeople;
    }
}
