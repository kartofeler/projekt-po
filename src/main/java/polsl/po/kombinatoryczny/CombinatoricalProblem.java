package polsl.po.kombinatoryczny;

import org.jacop.constraints.*;
import org.jacop.core.IntVar;
import org.jacop.core.Store;
import org.jacop.search.*;


/**
 * Created by Andrzej on 2014-12-08.
 */
public class CombinatoricalProblem {

    public CombinatoricalProblem() {

    }

    public void solveProblem(){
        Store store = new Store();

        int domainBound = 4;

        IntVar guest1 = new IntVar(store, "Tata", 1, 4);
        IntVar guest2 = new IntVar(store, "Mama", 1, 4);
        IntVar guest3 = new IntVar(store, "Kasia", 1, 4);
        IntVar guest4 = new IntVar(store, "Andrzej", 1, 4);

        IntVar[] v = {guest1, guest2, guest3, guest4};
        PrimitiveConstraint c1 = new Not(new XplusCeqZ(guest4, 1, guest3));
        PrimitiveConstraint c2 = new Not(new XplusCeqZ(guest4, -1, guest3));

        PrimitiveConstraint cz = new Not(new XplusCeqZ(guest4, domainBound - 1, guest3));
        PrimitiveConstraint cz2 = new Not(new XplusCeqZ(guest4, -(domainBound - 1), guest3));

        PrimitiveConstraint[] c = {c1, c2, cz, cz2};
        PrimitiveConstraint c12 = new And(c);

        PrimitiveConstraint c3 = new XplusCeqZ(guest1, 1, guest3);
        PrimitiveConstraint c4 = new XplusCeqZ(guest1, -1, guest3);

        PrimitiveConstraint cr = new XplusCeqZ(guest1, domainBound - 1, guest3);
        PrimitiveConstraint cr2 = new XplusCeqZ(guest1, -(domainBound - 1), guest3);

        PrimitiveConstraint[] co = {c3, c4, cr, cr2};
        PrimitiveConstraint c34 = new Or(co);

        store.impose(new Alldifferent(v));
        store.impose(c12);
        store.impose(c34);

        Search<IntVar> label = new DepthFirstSearch<IntVar>();
        label.getSolutionListener().searchAll(true);
        label.getSolutionListener().recordSolutions(true);
        
        SelectChoicePoint<IntVar> select = new InputOrderSelect<IntVar>(store,
                        v, new IndomainMin<IntVar>());
        boolean result = label.labeling(store, select);
        label.printAllSolutions();
    }
}

